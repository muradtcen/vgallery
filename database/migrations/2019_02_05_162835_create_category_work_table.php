<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_work', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('work_id')->unsigned();
            $table->timestamps();
        });

        $dump = file_get_contents(database_path('/resources/dump.sql'));
        \DB::unprepared(\DB::raw($dump));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_work');
    }
}
