<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class dumpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dump = file_get_contents(database_path('resources/dump.sql'));
        DB::unprepared(DB::raw($dump));
    }
}
