<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function works()
    {
        return $this->hasMany(Work::class);
    }

    public function get_author_name_by_id($author_id)
    {
        return $this->where('id', $author_id)->first();
    }

    public function getWorks()
    {
        return Work::where('author_id', $this->id)->paginate(25);
    }
}
