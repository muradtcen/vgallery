<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function works()
    {
        return $this->belongsToMany(Work::class);
    }

}
