<?php

namespace App\Http\Controllers;

use App\Http\Resources\AuthorResource;
use Illuminate\Http\Request;
use App\Models\Author;

class AuthorController extends Controller
{
    public function index()
    {
        return AuthorResource::collection(Author::paginate(25));
    }

    public function store(Request $request)
    {

    }

    public function show(Author $author)
    {
        return new AuthorResource($author);
    }

    public function update(Request $request, Author $author)
    {

    }

    public function destroy(Author $author)
    {
        $author->delete();

        return response()->json(null, 204);
    }
}
