<?php

namespace App\Http\Controllers;
use App\Models\Author;
use App\Models\Category;
use App\Http\Resources\WorkResource;
use App\Models\Work;
use Illuminate\Http\Request;

class WorkController extends Controller
{
    public function index(Author $author)
    {
        return WorkResource::collection($author->works()->get())->toArray(request());
    }

    public function indexByCategory(Category $category)
    {
        return WorkResource::collection($category->works()->get())->toArray(request());
    }

    public function store(Request $request)
    {

    }

    public function show(Author $author, Work $work)
    {
        return new WorkResource($work);
    }

    public function update(Request $request, Author $author, Work $work)
    {

    }

    public function destroy(Author $author, Work $work)
    {
        $work->delete();

        return response()->json(null, 204);
    }
}
