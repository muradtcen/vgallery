<?php

namespace App\Http\Controllers;

use App\Category;
use App\Work;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function show()
    {

    }

    public function show_about()
    {
        return view('site.about');
    }

    public function show_contacts()
    {
        return view('site.contacts');
    }

    public function show_wall()
    {
        return view('site.wall');
    }
}
