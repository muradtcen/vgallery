<?php

namespace App\Providers;

use App\Models\Author;
use App\Models\Category;
use App\Models\Work;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('author', function ($value, $route) {
            if (array_has($route->parameters, 'author')) {
                Route::bind('work', function ($value, $route) {
                    return Work::where('author_id', $route->parameters()['author']->id)->findOrFail($value);
                });
            }

            return Author::findOrFail($value);
        });

        Route::bind('category', function ($value, $route) {
            if (array_has($route->parameters, 'category')) {
                Route::bind('work', function ($value, $route) {
                    return Work::whereHas('categories', function ($q) use ($route) {
                        $q->where('id', $route->parameters()['category']->id);
                    })->findOrFail($value);
                });
            }

            return Category::findOrFail($value);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
